use std::os::raw;

#[no_mangle]
pub unsafe extern "C" fn vnice_bar_strdup(
    s_ptr: *const raw::c_char,
) -> *mut raw::c_char {
    if s_ptr.is_null() {
        return std::ptr::null_mut();
    }
    glib_sys::g_strdup(s_ptr)
}
