fn main() {
    cdylib_link_lines::metabuild();
    // Don't rebuild if meson build files or C sources/headers changed. Only when Rust code inside
    // src/ changes.
    println!("cargo:rerun-if-changed=src/");
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=Cargo.toml");
}
