#!/usr/bin/env python3

import os, sys, shutil
import os, sys, shutil
libdir = sys.argv[1]
src = sys.argv[2]
prefix = os.environ['MESON_INSTALL_DESTDIR_PREFIX']
dest = os.path.join(prefix, libdir, sys.argv[3])
if os.path.lexists(dest):
  os.remove(dest)
os.symlink(src, dest)
