# Usage

```sh
meson builddir/ --prefix=foo

meson compile -C builddir       # or ninja -C builddir

# You can also set DESTDIR as with Autotools
meson install -C builddir       # or ninja -C builddir install
```
